import React, { Component } from "react";
import inputcss from "./inputbox.css";

class InputBox extends Component {
  state = {};
  render() {
    return (
      <input
        className={"animated fadeInDown animation " + this.props.className}
        type="text"
        id={this.props.id}
        placeholder={this.props.placeholder}
        value={this.props.value}
        onChange={this.props.onChange}
        name={this.props.name}
      />
    );
  }
}

export default InputBox;
